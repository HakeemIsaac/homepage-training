$(document).ready(function(){

	// SMOOTH SCROLLING
	$("header nav ul li a[href='#about']").click(function(){
		$("html,body").animate({
			scrollTop: $("#about").offset().top
		}, "slow");
	});
	$("header nav ul li a[href='#education']").click(function(){
		$("html,body").animate({
			scrollTop: $("#education").offset().top
		}, "slow");
	});
	$("header nav ul li a[href='#skill']").click(function(){
		$("html,body").animate({
			scrollTop: $("#skill").offset().top
		}, "slow");
	});
	$("header nav ul li a[href='#work']").click(function(){
		$("html,body").animate({
			scrollTop: $("#work").offset().top
		}, "slow");
	});
	$("header nav ul li a[href='#contact']").click(function(){
		$("html,body").animate({
			scrollTop: $("#contact").offset().top
		}, "slow");
	});
	$("header nav ul li a[href='#others']").click(function(){
		$("html,body").animate({
			scrollTop: $("#others").offset().top
		}, "slow");
	});


	// SKILL SLIDER
	$("header nav ul li a[href='#skill']").click(function(){

		$("#html div").animate({
			"width": "80%"
		}, 3000);
		$("#css div").animate({
			"width": "70%"
		}, 3000);
		$("#js div").animate({
			"width": "65%"
		}, 3000);
		$("#php div").animate({
			"width": "60%"
		}, 3000);
	});

	// BURGER DROP DOWN
	$("#burger").click(function(){
		$("header nav").slideToggle();
	});

	setInterval(myFunction, 1000);

	function myFunction(){

		var browser_width = $(document).width();

		if(browser_width > 900){

			$("header nav").css({
				display: "block"
			});
		}
	}

});